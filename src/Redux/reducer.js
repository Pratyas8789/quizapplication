import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'

const initialState = {
    category: "9",
    difficulty: "easy",
    allQuestions: [],
    solvedQuestions: Array(10).fill(null),
    questionNo: 0,
    isLoading: false,
    score: 0,
    error: ""
}

export const getQustionsDifficulty = createAsyncThunk('allQuestion/getQustions', ({ difficulty, category }) => {
    try {
        return fetch(`https://opentdb.com/api.php?amount=10&category=${category}&difficulty=${difficulty}&type=multiple`)
            .then(res => res.json())
    }
    catch (error) {
        console.log(error);
    }
})

export const getQustionsCategory = createAsyncThunk('allQuestion/getQustions', ({ difficulty, category }) => {
    try {
        return fetch(`https://opentdb.com/api.php?amount=10&category=${category}&difficulty=${difficulty}&type=multiple`)
            .then(res => res.json())
    }
    catch (error) {
        console.log(error);
    }
})

const questionSlice = createSlice({
    name: "allQuestion",
    initialState,
    reducers: {
        handlelevel: (state, { payload }) => {
            state.difficulty = payload
        },
        nextQuestion: (state) => {
            state.questionNo = state.questionNo + 1
        },
        setCategory: (state, { payload }) => {
            state.category = payload
        },
        setAnswer: (state, { payload }) => {
            state.solvedQuestions[payload.index] = payload.ans
        },
        refreshState: (state) => {
            state.allQuestions = []
            state.solvedQuestions = Array(10).fill(null)
            state.questionNo = 0
        }
    },
    extraReducers: {
        [getQustionsDifficulty.pending]: (state) => {
            state.isLoading = true
        },
        [getQustionsDifficulty.fulfilled]: (state, { payload }) => {
            state.isLoading = false
            state.allQuestions = payload.results
        },
        [getQustionsDifficulty.rejected]: (state) => {
            state.isLoading = false
            state.error = "api is not worked"
        },
        [getQustionsCategory.pending]: (state) => {
            state.isLoading = true
        },
        [getQustionsCategory.fulfilled]: (state, { payload }) => {
            state.isLoading = false
            state.allQuestions = payload.results
        },
        [getQustionsCategory.rejected]: (state) => {
            state.isLoading = false
            state.error = "api is not worked"
        },
    }
})

export const { handlelevel, nextQuestion,refreshState, setCategory, setAnswer } = questionSlice.actions
export default questionSlice.reducer