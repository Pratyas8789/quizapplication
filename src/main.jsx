import React from 'react'
import ReactDOM from 'react-dom/client'
import App from './App.jsx'
import './index.css'
import { Provider } from 'react-redux'
import { store } from './Redux/store.js'
import Error from './Components/Erroroundaries.jsx'

ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
    <Error>
      <Provider store={store} >
        <App />
      </Provider>
    </Error>
  </React.StrictMode>,
)
