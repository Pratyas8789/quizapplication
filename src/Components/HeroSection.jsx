import React from 'react'
import QuizCategories from './QuizCategories'

export default function HeroSection() {
  return (
    <div className='d-flex flex-column align-items-center' >
        <h1 className='text-success' >Welcome to quiz application</h1>
        <QuizCategories/>
    </div>
  )
}
