import React from 'react'
import AllLevelCard from './AllLevelCard'
import { useParams } from 'react-router-dom'

export default function AllLevelContainer() {
  const categoriesName=useParams()

  return (
    <div className='d-flex flex-column align-items-center'>
      <h2 className='text-danger mt-3' >Welcome to {categoriesName.fname+" "+categoriesName.lname} Quiz</h2>
      <h3 className='mt-3'>Here we have three difficulty levels,go with each one and test your knowledge</h3>
      <h4 className='text-success mt-3' >Select the level of Quiz</h4>
      <AllLevelCard/>
    </div>
  )
}
