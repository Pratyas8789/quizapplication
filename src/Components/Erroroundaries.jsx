import React, { Component } from 'react'

export default class Error extends Component {
  constructor(props) {
    super(props);
    this.state = { hasError: false };
  }
  componentDidCatch(error, errorInfo) {
    console.log(error, errorInfo);
  }
  static getDerivedStateFromError(error) {
    return { hasError: true }
  }
  render() {
    if (this.state.hasError) {
      return <h3>Server is not working, please try again later</h3>;
    }
    return this.props.children;
  }
}