import React from 'react'
import { useSelector } from 'react-redux'
import { useParams } from 'react-router-dom'
import { TiTickOutline } from 'react-icons/ti';
import { RxCrossCircled } from 'react-icons/rx';

export default function QuizResult() {
    const { allQuestions, solvedQuestions } = useSelector(store => store.allQuestion)
    const parms = useParams()
    let score=0;
    let allresult
    if (Array.isArray(allQuestions) && allQuestions.length > 0) {
        allresult = allQuestions.map((data, index) => {
            if(data.correct_answer===solvedQuestions[index]){
                score++
            }
            return (
                <div className='d-flex' style={{ height: "80px" }} >
                    <div className='displayQuestion d-flex justify-content-center align-items-center text-center'>{data.question}</div>
                    <div className='displayanswer d-flex justify-content-center align-items-center'>{data.correct_answer}</div>
                    <div className='displayanswer d-flex justify-content-center align-items-center'>{solvedQuestions[index]}</div>
                    <div className='displayanswer d-flex justify-content-center align-items-center'>{(data.correct_answer === solvedQuestions[index]) ? <TiTickOutline className='text-success ' size={30} /> : <RxCrossCircled className='text-danger' size={30}/>}</div>
                </div>
            )
        })
    }
    return (
        <div className='d-flex flex-column align-items-center' >
            <img className='winnerCup' src="https://media.istockphoto.com/id/1168757141/vector/gold-trophy-with-the-name-plate-of-the-winner-of-the-competition.jpg?s=612x612&w=0&k=20&c=ljsP4p0yuJnh4f5jE2VwXfjs96CC0x4zj8CHUoMo39E=" alt="" srcset="" />
            <h3>Quiz Result</h3>
            <h5 className='d-flex'>Quiz Category:<h5 className='text-success' >{(parms.firstName + " " + parms.lastName).toUpperCase()}</h5> </h5>
            <h5 className='d-flex'>Quiz Difficulty Level: <h5 className='text-danger'>{(parms.level.toUpperCase())}</h5></h5>
            <h5>Final Score: {score}</h5>
            <div className='displayResult'>
                <div className='d-flex' style={{ height: "50px" }} >
                    <div className='displayQuestion d-flex justify-content-center align-items-center'>Question</div>
                    <div className='displayanswer d-flex justify-content-center align-items-center'>Correct Answers</div>
                    <div className='displayanswer d-flex justify-content-center align-items-center'>You Selected</div>
                    <div className='displayanswer d-flex justify-content-center align-items-center'>Right Or Wrong</div>
                </div>
                {allresult}
            </div>
        </div>
    )
}
