import React from 'react'
import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';
import { useDispatch } from 'react-redux';
import { Link, useParams } from 'react-router-dom';
import { handlelevel } from '../Redux/reducer';


export default function AllLevelCard() {
    const parms = useParams()
    const dispatch = useDispatch()
    const level = [{ name: "Easy Level", color: "light", level: "easy" }, { name: "Medium Level", color: "success", level: "medium" }, { name: "Hard Level", color: "danger", level: "hard" },]
    let levelCard
    levelCard = level.map((eachLevel) => {
        return (
            <Card key={eachLevel.color} className='bg-warning m-3' style={{ width: '18rem' }}>
                <Card.Body className='d-flex flex-column align-items-center '>
                    <Card.Title className={`text-${eachLevel.color}`} >{eachLevel.name}</Card.Title>
                    <Card.Text className='text-center text-danger' >
                        This quiz will test you the knowledge of {parms.firstName + " " + parms.lastName}
                    </Card.Text>
                    <Link to={`${eachLevel.level}`} >
                        <Button variant="dark" onClick={() => dispatch(handlelevel(eachLevel.level))} >Take a quiz</Button>
                    </Link>
                </Card.Body>
            </Card>
        )
    })
    return (
        <div className='d-flex' >{levelCard}</div>
    );
}
