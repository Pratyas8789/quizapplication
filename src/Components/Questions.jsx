import React from 'react'
import { Link, useParams } from 'react-router-dom'
import Button from 'react-bootstrap/Button';
import { useDispatch, useSelector } from 'react-redux';
import { nextQuestion, setAnswer } from '../Redux/reducer';

export default function Questions() {
    const { questionNo, allQuestions, solvedQuestions } = useSelector(store => store.allQuestion)
    const params = useParams()
    const dispatch = useDispatch()
    let questionDisplay
    if (allQuestions !== undefined && allQuestions.length > 0) {
        questionDisplay =
            <>
                <h4>{questionNo + 1}) {allQuestions[questionNo].question}</h4>
                <div className='d-flex flex-column'>
                    <Button onClick={() => dispatch(setAnswer({ ans: allQuestions[questionNo].correct_answer, index: questionNo }))} className={`w-50 m-3 ${solvedQuestions[questionNo] == allQuestions[questionNo].correct_answer ? "bg-dark text-light" : "bg-warning"}`} variant="warning">{allQuestions[questionNo].correct_answer}</Button>{' '}
                    <Button onClick={() => dispatch(setAnswer({ ans: allQuestions[questionNo].incorrect_answers[0], index: questionNo }))} className={`w-50 m-3 ${solvedQuestions[questionNo] == allQuestions[questionNo].incorrect_answers[0] ? "bg-dark text-light" : "bg-warning"}`} variant="warning">{allQuestions[questionNo].incorrect_answers[0]}</Button>{' '}
                    <Button onClick={() => dispatch(setAnswer({ ans: allQuestions[questionNo].incorrect_answers[1], index: questionNo }))} className={`w-50 m-3 ${solvedQuestions[questionNo] == allQuestions[questionNo].incorrect_answers[1] ? "bg-dark text-light" : "bg-warning"}`} variant="warning">{allQuestions[questionNo].incorrect_answers[1]}</Button>{' '}
                    <Button onClick={() => dispatch(setAnswer({ ans: allQuestions[questionNo].incorrect_answers[2], index: questionNo }))} className={`w-50 m-3 ${solvedQuestions[questionNo] == allQuestions[questionNo].incorrect_answers[2] ? "bg-dark text-light" : "bg-warning"}`} variant="warning">{allQuestions[questionNo].incorrect_answers[2]}</Button>{' '}
                </div>
            </>
    }
    const handleClick = () => {
        if (solvedQuestions[questionNo] == null) {
            alert("please  select one option")
        } else {
            dispatch(nextQuestion())
        }
    }
    return (
        <>
            <div className='d-flex justify-content-center'  >
                <div className='w-75 ' >
                    <div className='d-flex justify-content-around mt-4' >
                        <Button variant="warning">Quiz Category: {params.fname + " " + params.lname}</Button>
                        <Button variant="danger">Quiz Level {params.level}</Button>{' '}
                    </div>
                    <div className='mt-5 mb-5' >
                        Question {questionNo + 1}/10
                        <div className='progressBarDv' >
                            <div className='progressBar' style={{ width: `${(questionNo + 1) * 10}%` }}></div>
                        </div>
                    </div>
                    <div  >
                        {questionDisplay}
                    </div>
                    <div className='d-flex justify-content-end'>
                        {questionNo < 9 ? <Button variant="dark" onClick={handleClick} >Next Question</Button> :
                            <>
                                {solvedQuestions[questionNo] == null ? <Button variant="dark" onClick={handleClick}>submit</Button> :
                                    <Link to='result'>
                                        <Button variant="dark" onClick={handleClick}>submit</Button>
                                    </Link>}

                            </>}
                    </div>
                </div>
            </div>
        </>
    )
}
