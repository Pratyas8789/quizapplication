import React from 'react'
import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';
import { useDispatch } from 'react-redux';
import { Link } from 'react-router-dom';
import { setCategory } from '../Redux/reducer';

export default function QuizCategories() {
    const dispatch = useDispatch()
    const categories = ["General Knowledge", "Entertainment Books", "Entertainment Film", "Entertainment Music", "Entertainment Television"]
    const image = ["https://static.javatpoint.com/gk/images/gk.png", "https://thesocialtalks.com/ezoimgfmt/bucket-thesocialtalks.s3.amazonaws.com/static/article/2022/08/29/books.jpg?ezimgfmt=ngcb4/notWebP", "https://imageio.forbes.com/specials-images/imageserve/1206859285/Tom-Hanks---Rita-Wilson/960x0.jpg?format=jpg&width=960", "https://images.shiksha.com/mediadata/images/articles/1585221480phpX5Vsxq.jpeg", "https://m.economictimes.com/thumb/msid-59452071,width-1200,height-900,resizemode-4,imgsize-67106/46-million-households-to-use-free-tv-by-2020.jpg"]
    let allCategories;
    const category = [9, 10, 11, 12, 14]
    allCategories = categories.map((eachCategories, index) => {
        return (
            <Card key={index} className=' categoriesContainer d-flex flex-row shadow rounded-5 m-3 w-50'>
                <Card.Img className='w-25 h-100 rounded-5' variant="top" src={image[index]} />
                <Card.Body className='w-75 h-100'>
                    <Card.Title>{eachCategories}</Card.Title>
                    <Card.Text>
                        This quiz will test you the knowledge of {eachCategories}
                    </Card.Text>
                    <Link to={`${eachCategories.replaceAll(' ', '/')}`}>
                        <Button variant="primary" onClick={() => dispatch(setCategory(category[index]))}>Take a quiz</Button>
                    </Link>
                </Card.Body>
            </Card>
        )
    })
    return (
        <>
            {allCategories}
        </>
    )
}
