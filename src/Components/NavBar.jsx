import React from 'react'
import Container from 'react-bootstrap/Container';
import Navbar from 'react-bootstrap/Navbar';
import { useDispatch } from 'react-redux';
import { Link } from 'react-router-dom';
import { refreshState } from '../Redux/reducer';

export default function NavBar() {
  const dispatch=useDispatch()
  return (
    <Navbar bg="warning" expand="lg">
      <Container fluid>
        <Link className='text-decoration-none' to='/' onClick={()=>dispatch(refreshState())}>
          <Navbar.Brand>Quiz app</Navbar.Brand>
        </Link>
        <Link className='text-decoration-none' to='/'onClick={()=>dispatch(refreshState())} >
          <Navbar.Brand>Home</Navbar.Brand>
        </Link>
      </Container>
    </Navbar>
  )
}
