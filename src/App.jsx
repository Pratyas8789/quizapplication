import { Suspense, lazy, useEffect } from 'react'
import './App.css'
import 'bootstrap/dist/css/bootstrap.min.css';
import NavBar from './Components/NavBar';
import HeroSection from './Components/HeroSection';
import { Route, BrowserRouter as Router, Routes } from 'react-router-dom'
import AllLevelContainer from './Components/AllLevelContainer';
import { getQustionsCategory, getQustionsDifficulty } from './Redux/reducer';
import { useDispatch, useSelector } from 'react-redux'
import QuizResult from './Components/QuizResult';
import Spinner from './Components/Spinners';
import ErrorPage from './Components/ErrorPage';

const Question = lazy(() => import('./Components/Questions'))

function App() {
  const { category, difficulty, error } = useSelector(store => store.allQuestion)
  const dispatch = useDispatch()
  useEffect(() => {
    dispatch(getQustionsDifficulty({ "difficulty": difficulty, "category": category }))
  }, [difficulty])

  useEffect(() => {
    dispatch(getQustionsCategory({ "difficulty": difficulty, "category": category }))
  }, [category])

  if (error) {
    throw new Error(error.message);
  }
  return (
    <div className='App'>
      <Router>
        <NavBar />
        <Routes>
          <Route exact path='/' element={<HeroSection />} />
          <Route exact path='/:firstName/:lastName' element={<AllLevelContainer />} />
          <Route exact path='/:firstName/:lastName/:level' element={<Suspense fallback={<h1>Please wait... <Spinner /></h1>}><Question /></Suspense>}></Route>
          <Route exact path='/:firstName/:lastName/:level/:result' element={<QuizResult />} />
          <Route exact path='*' element={<ErrorPage />} />
        </Routes>
      </Router>
    </div>
  )
}
export default App